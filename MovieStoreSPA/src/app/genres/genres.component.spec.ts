import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { GenreService } from '../core/services/genre.service';
import { Genre } from '../shared/models/genre';

import { GenresComponent } from './genres.component';

fdescribe('GenresComponent', () => {
  var comp = null;
  var genreService = null;
  class MockGenresService {
    isLoggedIn = true;
    getAllGenres():Observable<Genre[]>{
      //http://localhost:54655/api/genres
     return new Observable<Genre[]>();
   }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      // provide the component-under-test and dependent service
      providers: [
        GenresComponent,
        { provide: GenreService, useClass: MockGenresService }
      ]
    });
    // inject both the component and the dependent service.
   comp = TestBed.inject(GenresComponent);
    genreService = TestBed.inject(GenreService);
  });

    it('should not have welcome message after construction', () => {
      expect(comp.welcome).toBeUndefined();
    });
    
    it('should welcome logged in user after Angular calls ngOnInit', () => {
      comp.ngOnInit();
      expect(comp.welcome).toContain('Welcome');
    });
    
    it('should ask user to log in if not logged in after ngOnInit', () => {
      genreService.isLoggedIn= false;
      comp.ngOnInit();
      expect(comp.welcome).not.toContain('Welcome');
      expect(comp.welcome).toContain('log in');
    });
});
